# Mettre en place un vpn grâce à un vpn

**Prérequis**
 * Une connection internet assez élévée
 * Une raspberry pi 4 (pas de préférrence pour la ram je pense (j'en ai 8 GB))
 * OS: raspberry pi os 32-bit

## 1.PREPARATION ##

### preparation de la raspberry ###

**établir une connection entre votre ordinateur et la raspberry**
Pour commencer il va falloir récuperer l'addresse ip de la raspberry, pour ça, entrons 
```bash 
pi@raspberrypi:~ $ ip a
``` 
et on récupere ce qui nous interesse 
  ```bash
  inet 192.168.1.32/24
  ```

Depuis notre ordinateur
```bash
PS C:\Users\bravo> ssh pi@192.168.1.32
```
et voilà
  ```bash
  PS C:\Users\bravo> ssh pi@192.168.1.32
Linux raspberrypi 5.10.92-v7l+ #1514 SMP Mon Jan 17 17:38:03 GMT 2022 armv7l

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Sun Feb 20 14:02:49 2022 from 192.168.1.27
pi@raspberrypi:~ $
```
**mettre à jour la raspberry**
```bash
pi@raspberrypi:~ $ sudo apt update
```

    Hit:1 http://raspbian.raspberrypi.org/raspbian bullseye InRelease
    Hit:2 http://archive.raspberrypi.org/debian bullseye InRelease
    Reading package lists... Done
    Building dependency tree... Done
    Reading state information... Done
    14 packages can be upgraded. Run 'apt list --upgradable' to see them.
    
---    

```bash
pi@raspberrypi:~ $ sudo apt full-upgrade
```

## 2.Mise en place du vpn ##

**installation de wireguard**
```bash
pi@raspberrypi:/ $ echo "deb http://archive.raspbian.org/raspbian testing main" | sudo tee --append /etc/apt/sources
```
```bah
pi@raspberrypi:/ $ sudo apt update
```
```bash
pi@raspberrypi:/ $ sudo apt install wireguard -y
```

On verifie que tout est bien téléchargé
```bash 
pi@raspberrypi:/ $ which wg wg-quick
```
  ```bash
  /usr/bin/wg
  /usr/bin/wg-quick
  ```

**Verifier si tout est bien installé**
```bash
pi@raspberrypi:/ $ sudo touch /etc/wireguard/wg0.conf
pi@raspberrypi:/ $ sudo wg-quick up wg0
[#] ip link add wg0 type wireguard
[#] wg setconf wg0 /dev/fd/63
[#] ip link set mtu 1420 up dev wg0
```

**installer le script de gestion des utilisateurs**
- On installe ce qui va générer les qr code
```bash
pi@raspberrypi:/ $ sudo apt install qrencode -y
```
-On telecharge le reste
```bash
pi@raspberrypi:/ $ wget https://github.com/adrianmihalko/wg_config/archive/master.zip
```
```bash
pi@raspberrypi:~ $ wget https://github.com/adrianmihalko/wg_config/archive/master.zip
--2020-07-02 19:00:52--  https://github.com/adrianmihalko/wg_config/archive/master.zip
Resolving github.com (github.com)... 140.82.112.4
...
2020-07-02 19:00:53 (2.60 MB/s) - ‘master.zip’ saved [3688/3688]
```
```bash
pi@raspberrypi:~ $ unzip -j master.zip -d wg_config
```
```bash
pi@raspberrypi:~ $ wget https://github.com/adrianmihalko/wg_config/archive/master.zip
:~ $ mkdir downloads
pi@raspberrypi:~ $ wget https://github.com/adrianmihalko/wg_config/archive/master.zip
:~ $ mv master.zip downloads/wg_config_script.zip
```

**Générer clé privée et public**

ça marche plus mais voilà la demarche
```bash
pi@raspberrypi:~ $ wg genkey > server_private.key
pi@raspberrypi:~ $ wg pubkey > server_public.key < server_private.key
```
puis on les verifie grâce à
```bash
pi@raspberrypi:/wg_config $cat server_public.key
```
et
```bash
pi@raspberrypi:/wg_config $cat server_private.key
```

**On modifie le fichier de definition de serveur**
```bash
pi@raspberrypi:~/wg_config $ cp wg.def.sample wg.def
pi@raspberrypi:/wg_config $ nano wg.def
```
et on esst censé ajouter les clés aux endroits requis pour


**Création d'utilisateurs**
on entre la commande d'utilisateur
```bash
sudo ./user.sh -a val
```
on modifie les fichiers users/val/client.conf et wireguard/wg0.conf en les faisant correspondre puis users/val/client.all.conf (pensez à bien modifié le endpoint)


**Generer le qrcode**
on utiliser
```bash
sudo ./user.sh -v val
```
qui affiche un qrcode qu'on scan depuis l'applications wireguard du telephone.
